function layThongTinTuForm() {
    var maSv = document.getElementById('txtMaSV').value;
    var tenSv = document.getElementById('txtTenSV').value;
    var email = document.getElementById('txtEmail').value;
    var matKhau = document.getElementById('txtPass').value;
    var diemLy = document.getElementById('txtDiemLy').value*1;
    var diemHoa = document.getElementById('txtDiemHoa').value*1;
    var diemToan = document.getElementById('txtDiemToan').value*1;
    var sv = new Sinhvien(maSv,tenSv,email,matKhau,diemLy,diemHoa,diemToan);
    return sv;
}
function renderDssv(list) {
    var contentHTML = '';
    for (var i = 0; i < list.length; i++) {
        var currentSv = list[i];
        var contentTr = `<tr>
        <td>${currentSv.ma}</td>
        <td>${currentSv.ten}</td>
        <td>${currentSv.email}</td>
        <td>0</td>
        <td>
        <button onclick="xoaSV('${currentSv.ma}')" class=" btn btn-danger">Xóa</button>
        <button onclick="suaSV('${currentSv.ma}')" class=" btn btn-primary">Sửa</button>
        </td>
        </tr>
        `;
        contentHTML = contentHTML + contentTr;
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}
// function showThongTinLenForm(sv) {
//     document.getElementById('txtMaSV').value = sv.ma;
//     document.getElementById('txtTenSV').value = sv.ten; 
//     document.getElementById('txtEmail').value = sv.email;
//     document.getElementById('txtPass').value = sv.matKhau;
//     document.getElementById('txtDiemLy').value = sv.diemLy;
//     document.getElementById('txtDiemHoa').value = sv.diemHoa;
//     document.getElementById('txtDiemToan').value = sv.diemToan;
// }