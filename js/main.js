// CRUD
var dssv = [];
const DSSV = 'DSSV';
var dataJson = localStorage.getItem(DSSV);
if (dataJson) {
    //truthy falsy
    dssv = JSON.parse(dataJson);
    renderDssv(dssv);
}
function saveLocalStorage() {
        var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson); 
}
function themSv() {
    var newSv = layThongTinTuForm();
    dssv.push(newSv);
    //lưu xuống local
    saveLocalStorage();
    renderDssv(dssv);
    document.getElementById("formQLSV").reset();
}

function xoaSV(idSV) {
    //indexOf ~~ phần tử là string, number
    //findIndex ~~ xử lý phần tử là object
    //tìm vị trí
    var index = dssv.findIndex(function (sv) {
        return sv.ma == idSV;
    })
    if (index == -1) {
        return;
    }
    //xóa phần tử khỏi danh sách
    dssv.splice(index,1);
    // sau khi xóa thì dữ liệu thay đổi, nhưng layout không có thay đổi vì layout được tạo ra nhờ renderDssv
    //update dữ liệu cho localstorage
    // saveLocalStorage();
    renderDssv(dssv);
}
function suaSV(idSV) {
    let index = dssv.findIndex(function(sv) {
        return sv.ma = idSV;
    })
    if (index == -1) return;
    var sv = dssv[index];
    console.log(sv);
    // showThongTinLenForm(sv);
}
// function capNhatSv() {
//     var svEdit = layThongTinTuForm();
//     console.log(svEdit);
// }
//localstorage: chỉ lưu được json   



// null, undefined, false, "", NaN, 0